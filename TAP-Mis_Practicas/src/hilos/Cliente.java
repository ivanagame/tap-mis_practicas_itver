package hilos;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author LaboratorioU005_11
 */
public class Cliente {
    
    public static void main(String args[]){
        
        Socket cliente;
                
        try {
            cliente = new Socket("10.25.0.156",1001);
                         
            PrintWriter out =
                new PrintWriter(cliente.getOutputStream(), true);
            
            BufferedReader in =
                new BufferedReader(
                    new InputStreamReader(cliente.getInputStream()));
            
            BufferedReader stdIn =
                new BufferedReader(
                    new InputStreamReader(System.in));
            
            String saludoInicial = stdIn.readLine();
            
            out.println(saludoInicial);
            
            System.out.println(in.readLine());
            
            cliente.close();
            
        } catch (IOException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
        }                        
    }    
}

