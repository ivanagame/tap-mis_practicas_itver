package hilos;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Ejemplo1 implements Runnable{
    public static void main(String[] args) {
         java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Ejemplo1().run();
            }
        });
        new Tarea("Tarea 1",500).start();
        new Tarea("Tarea 2",1000).start();
        new Tarea("Tarea 3",2000).start();
    }
    @Override
    public void run() {
        for (int i = 0; i < 20; i++) {
            System.out.println(" Ejecucion principal ");
            
            try {
                Thread.sleep(1000);
                System.out.println("Ejecucion principal,contador: "+i);
            } catch (InterruptedException ex) {
                Logger.getLogger(Ejemplo1.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
class Tarea extends  Thread{
String id="";
int miliseg=1000;
    public Tarea(String id,int miliseg) {
    this.id = id;
    this.miliseg = miliseg;
    }
    
    
    @Override
    public void run() {
    for (int i = 0; i < 20; i++) {
            try {
                Thread.sleep(miliseg);
                System.out.printf("Ejecucion de tarea %s,contador: %d\n",this.id,i);
            } catch (InterruptedException ex) {
                Logger.getLogger(Ejemplo1.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
}
