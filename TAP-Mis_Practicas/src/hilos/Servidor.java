/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hilos;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author LaboratorioU005_11
 */
public class Servidor {
    
    int i = 0;
    
    ArrayList<Conexion> conexiones = null;
    
    public static void main(String args[]){
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {                   
                new Servidor().run();;
            }
        });                      
    }   
    
    public void run(){
        ServerSocket server = null;
        Socket       conn;
        System.out.println("Iniciando proceso servidor...");
            
        try {
            server = new ServerSocket(1001);
        } catch (Exception e){

        }            

        while (server!=null){                                
            try {  
                
                Socket s = server.accept();
                Conexion c = new Conexion(s,this.i++);
                conexiones.add(c); 
                c.start();
                
            } catch (IOException ex) {
                Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
            } finally{
                // server.close();            
            } 
        }      
    }
    
}

class Conexion extends Thread {
        Socket s;
        int id;
        boolean conectado = false;
        
        PrintWriter out = null;
        BufferedReader in = null;
        BufferedReader stdIn = null; 
        
        public Conexion(Socket _s, int _id){
            this.s = _s;
            this.id = _id;
            
            try {
                out = new PrintWriter(this.s.getOutputStream(), true);

                in = new BufferedReader(
                            new InputStreamReader(this.s.getInputStream()));

                stdIn = new BufferedReader(new InputStreamReader(System.in)); 
                
                this.conectado = true;
                
            } catch (IOException ioe){
                System.out.println(ioe.getMessage());
            }
        } 
            
        @Override
        public void run() {
            String saludo;
            
            System.out.printf("Se acepta conexion desde %s \n", this.s.getInetAddress());
            
            try {
                saludo = in.readLine();
                
                System.out.printf("Se recibe cadena de %s, con el contenido: %s \n",this.s.getInetAddress(),saludo);

                String respuesta = this.stdIn.readLine();                

                out.println(respuesta+" : "+this.s.getInetAddress());

                this.s.close();                 
                
            } catch (IOException ex) {
                Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
            } 
        }
    } 